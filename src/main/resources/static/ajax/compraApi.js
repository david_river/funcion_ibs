$(document).ready(inicio);
var apiProveedor = "/proveedor/";
var apiProductos = "/producto/";

let proveedores = {
    id: 0,
    nombre: ""
};

let producto = {
    id: 0,
    nombre: ""
};

function inicio() {
    $("#buscarProveedor").click(function () {
        $('#modalProveedor').modal('show');
        listarProveedores();
    });

    $("#buscarProducto").click(function () {
        $('#modalProductos').modal('show');
        listarProductos();
    });

} 

//MODAL AGREGAR CLIENTE A LA VENTA
function seleccionarUsuario(id) {
    $.ajax({
        url: apiProveedor + id,
        method: "post",
        success: function (response) {
            $("#proveedor").val(response.username);
            producto.id = response.id;
            $('#modalProveedor').modal('hide');
        },
        error: function (response) {
            alert("Error al obtener el dato");
        }
    });
}

function seleccionarProducto(id) {
    $.ajax({
        url: apiProductos + id,
        method: "post",
        success: function (response) {
            $("#producto").val(response.nombre);
            producto.id = response.id;
            $('#modalProductos').modal('hide');
        },
        error: function (response) {
            alert("Error al obtener el dato");
        }
    });
}

function listarProveedores() {
    $("#tablaProveedores").DataTable({
        "ajax": {
            "url": apiProveedor,
            "method": "post"
        },
        "columns": [{
            "data": "username",
            "width": "20%"
        }, {
            "data": "direccion",
            "width": "20%"
        }, {
            "data": "dui",
            "width": "20%"
        }, {
            "data": "telefono",
            "width": "20%"
        }, {
            "data": "correo",
            "width": "20%"
        }, {
            "data": "seleccionar",
            "width": "5%"
        }],

        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "bDestroy": "true"
    });
}

function listarProductos() {
    $("#tablaProductos").DataTable({
        "ajax": {
            "url": apiProductos,
            "method": "post"
        },
        "columns": [{
            "data": "codigo",
            "width": "5%"
        }, {
            "data": "nombre",
            "width": "20%"
        }, {
            "data": "presentacion",
            "width": "20%"
        }, {
            "data": "marca",
            "width": "20%"
        }, {
            "data": "categoria",
            "width": "20%"
        }, {
            "data": "seleccionar",
            "width": "5%"
        }],

        "language": {
            "lengthMenu": "Mostrar _MENU_ ",
            "zeroRecords": "Datos no encontrados",
            "info": "Mostar páginas _PAGE_ de _PAGES_",
            "infoEmpty": "Datos no encontrados",
            "infoFiltered": "(Filtrados por _MAX_ total registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primero",
                "last": "Anterior",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "bDestroy": "true"
    });
}