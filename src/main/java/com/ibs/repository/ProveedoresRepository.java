package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ibs.entities.Proveedores;

@Repository
public interface ProveedoresRepository extends CrudRepository<Proveedores, Long>{

}
