package com.ibs.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ibs.entities.Usuarios;

@Repository
public interface UsuariosRepository extends CrudRepository<Usuarios, Long>{

}
