package com.ibs.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * The persistent class for the roles database table.
 * 
 */
@Entity
@Table(name = "roles")
public class Role implements Serializable {
  private static final long serialVersionUID = 1L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id", nullable = false, updatable = false, unique = true)
  private Long id;

  @NotNull
  @Column(nullable = false)
  @NotBlank(message = "rol no puede ir vacio")
  private String rol;

  public Role() {
  }

  public Role(String rol) {
    this.rol = rol;
  }

  public Role(Long id, String rol) {
    this.id = id;
    this.rol = rol;
  }

  public Long getId() {
    return this.id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getRol() {
    return this.rol;
  }

  public void setRol(String rol) {
    this.rol = rol;
  }
  
  public static Role fromId(Long id) {
    Role entity = new Role();
    entity.id = id;
    return entity;
  }

}