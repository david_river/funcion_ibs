package com.ibs.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "ventas")
public class Ventas implements Serializable {
	private static final long serialVersionUID = 1L;

	public enum TipoVenta {
		CF, CCF;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, updatable = false, unique = true)
	private Long id;

	@Column(nullable = false, length = 30)
	// @NotBlank(message = "nombre no puede ir vacio")
	private String nombre;

	@NotNull
	@Column(nullable = false, length = 30)
	// @NotBlank(message = "numeroFactura no puede ir vacio")
	private int numeroFactura;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 30, columnDefinition = "ENUM('CF', 'CCF')")
	private TipoVenta tipoVenta;

	// @NotNull
	@Column(name = "totalVenta", nullable = false, length = 20)
	// @NotBlank(message = "totalVenta no puede ir vacio")
	private double totalVenta;

	// bi-directional many-to-one association to Cliente
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idCliente", nullable = false)
	@JsonIdentityReference(alwaysAsId = true)
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
	@JsonProperty("idCliente")
	private Clientes cliente;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "ventas")
	private List<Detalleventa> detalleVenta;

	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idUsuario", nullable = false)
	@JsonIdentityReference(alwaysAsId = true)
	@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
	@JsonProperty("idUsuario")
	private Usuarios usuario;

	@Column(nullable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@JsonFormat(pattern = "yyyy-MM-dd HH-mm")
	private Date fechaVenta;

	public Ventas() {
	}

	public Ventas(Long id, String nombre, int numeroFactura, TipoVenta tipoVenta, double totalVenta, Clientes cliente,
			List<Detalleventa> detalleVenta, Usuarios usuario, Date fechaVenta) {
		this.id = id;
		this.nombre = nombre;
		this.numeroFactura = numeroFactura;
		this.tipoVenta = tipoVenta;
		this.totalVenta = totalVenta;
		this.cliente = cliente;
		this.detalleVenta = detalleVenta;
		this.usuario = usuario;
		this.fechaVenta = fechaVenta;
	}

	public Ventas(String nombre, int numeroFactura, TipoVenta tipoVenta, double totalVenta, Clientes cliente,
			List<Detalleventa> detalleVenta, Usuarios usuario, Date fechaVenta) {
		this.nombre = nombre;
		this.numeroFactura = numeroFactura;
		this.tipoVenta = tipoVenta;
		this.totalVenta = totalVenta;
		this.cliente = cliente;
		this.detalleVenta = detalleVenta;
		this.usuario = usuario;
		this.fechaVenta = fechaVenta;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getNumeroFactura() {
		return this.numeroFactura;
	}

	public void setNumeroFactura(int numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	public String getNombre() {
		return nombre;
	}

	public TipoVenta getTipoVenta() {
		return tipoVenta;
	}

	public double getTotalVenta() {
		return this.totalVenta;
	}

	public void setTotalVenta(double totalVenta) {
		this.totalVenta = totalVenta;
	}

	public Clientes getCliente() {
		return cliente;
	}

	@JsonProperty("idCliente")
	public void setClienteById(Long id) {
		cliente = Clientes.fromId(id);
	}

	@JsonIgnore
	public void setCliente(Clientes cliente) {
		this.cliente = cliente;
	}

	public Usuarios getUsuario() {
		return usuario;
	}

	@JsonProperty("idUsuario")
	public void setUsuarioById(Long id) {
		usuario = Usuarios.fromId(id);
	}

	@JsonIgnore
	public void setUsuario(Usuarios usuario) {
		this.usuario = usuario;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setTipoVenta(TipoVenta tipoVenta) {
		this.tipoVenta = tipoVenta;
	}

	public Date getFechaVenta() {
		return fechaVenta;
	}

	public void setFechaVenta(Date fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	public List<Detalleventa> getDetalleVenta() {
		return this.detalleVenta;
	}

	public void setDetalleVenta(List<Detalleventa> detalleVenta) {
		this.detalleVenta = detalleVenta;
	}

}