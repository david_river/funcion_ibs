package com.ibs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import com.ibs.repository.RoleRepository;

@Controller
public class ViewController {

  @Autowired
  RoleRepository rpRoles;
/* Redirege al login */
	/*
	 * @GetMapping(value = "login") public String login() { return
	 * "vistas/Login/login"; }
	 */

  /** Redirige_vista_listar */
  @GetMapping(value = "roles")
  public String roles() {
    return "vistas/Rol/Listar";
  }

  /** Redirige_vista_listar */
  @GetMapping(value = "marcas")
  public String marcas() {
    return "vistas/Marcas/Listar";
  }

  /** Redirige_vista_listar */
  @GetMapping(value = "categorias")
  public String categotias() {
    return "vistas/Categorias/Listar";
  }

  /** Redirige_vista_listar */
  @GetMapping(value = "usuarios")
  public String user(ModelMap mp) {
    mp.put("list", rpRoles.findAll());
    return "vistas/User/Listar";
  }

  /** Redirige_vista_listar */
  @GetMapping(value = "proveedores")
  public String proveedores() {
    return "vistas/Proveedores/Listar";
  }

  /** Redirige_vista_listar */
  @GetMapping(value = "inventario")
  public String inventario() {
    return "vistas/inventario/Listar";
  }

   /** Redirige_vista_listar */
   @GetMapping(value = "movimiento")
   public String movimiento() {
     return "vistas/Movimiento/Listar";                       
   }
   @GetMapping(value = "inicio")
   public String inicio() {
     return "vistas/Inicio/index";
   }

  @GetMapping(value = "clientes")
  public String cliente() {
    return "vistas/Clientes/Listar";
  }

  /** Redirige_vista_listar */
  @GetMapping(value = "productos")
  public String productos() {
    return "vistas/Productos/Listar";
  }

  @GetMapping(value = "ventas")
  public String nuevaVenta() {
    return "vistas/Ventas/agregarVenta";
  }

  @GetMapping(value = "compras")
  public String nuevaCompra() {
    return "vistas/Compras/agregarCompra";
  }
}
